---
title: "Couleurs"
author: "E. Morand"
date: "25/04/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Faire des graphiques




## Choix du graphique

[datatoviz](https://www.data-to-viz.com/)

## Choix des couleurs
A partir des couleurs de peinture [MetBrewer](https://github.com/BlakeRMills/MetBrewer)

## Les couleurs et les cultures
https://informationisbeautiful.net/visualizations/colours-in-cultures/

# Des sites utiles sur R

- [R bloggers](https://www.r-bloggers.com/)
- [R weekly](https://rweekly.org/)