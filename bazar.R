

```{r}
g1 <- bilan %>%
  ggplot(aes(x = CDD, xend = Fonctionnaire, y = Annee)) +
  geom_dumbbell(
    colour="#a3c4dc",
    colour_xend="#0e668b",
    size=4.0,
    dot_guide=TRUE,
    dot_guide_size=0.15,
    dot_guide_colour = "grey60"
  )
ggplotly(g1)
```


```{r}
df<-bilan%>%
  pivot_longer(cols=2:3,names_to="statut", values_to="ETP")

df <- df %>%
  
  mutate(paires = rep(1:(n()/2),each=2),
         
         statut=factor(statut))


p <- df %>% 
  
  ggplot(aes(x= ETP, y=Annee)) +
  
  geom_line(aes(group = paires),color="grey")+
  
  geom_point(aes(color=statut), size=4) +
  
  theme(legend.position="top")
```


```{r}
p <- bilan %>% 
  
  ggplot(aes(x = CDD, xend = Fonctionnaire, y = Annee)) +
  
  geom_point(aes(color=c("CDD","Fonctionnaire")), size=4) +
  
  theme_classic()



ggplotly(p)

```

